﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace ChatApp
{
    public partial class Form1 : Form
    {
        Socket sck;
        EndPoint EP_Local, EP_Remote;
        public Form1()
        {
            InitializeComponent();

            sck = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp); //soket oluştur
            sck.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);//soket özellikleri

            textLocalIp.Text = GetLocalIP(); //Bilgisayarın local adresi formdaki IP yerine yazılacak.
            textFriendsIp.Text = GetLocalIP(); //Tek bir bilgisyardan çalıştığımız için aynı IP adresi karşı tarafa verilecek.
        }

        private string GetLocalIP() //Host bilgisayarın IP adresini al
        {
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());

            foreach(IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "127.0.0.1"; //return değeri bulunamazsa bu host adresini döndür
        }
        private void MessageCallBack(IAsyncResult aResult)
        {
            try
            {
                int size = sck.EndReceiveFrom(aResult, ref EP_Remote);

                if (size > 0)//Bilgi geldiyse eğer;
                {
                    byte[] receivedData = new byte[1464];
                    receivedData = (byte[])aResult.AsyncState; //veriyi al
                    //byteları stringe çevirelim;
                    ASCIIEncoding eEncoding = new ASCIIEncoding();
                    string receivedMessage = eEncoding.GetString(receivedData);

                    //listbox ta gelen string veriyi gösterelim;
                    listBox1.Items.Add("A: "+receivedMessage);
                }
                byte[] buffer = new byte[1500];

                sck.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref EP_Remote,  //gelen her iistek için recursive çağrı.
                                        new AsyncCallback(MessageCallBack), buffer);
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e) //start butonu
        {
            try
            {
                //...Binding...
                                            //Client1'in IP adresini al                //Client1'in port adresini al
                EP_Local = new IPEndPoint(IPAddress.Parse(textLocalIp.Text), Convert.ToInt32(textLocalPort.Text));
                sck.Bind(EP_Local);  
                 
                //...Connecting...
                                            //Client2'nin IP adresini al                //Client2'nin port adresini al
                EP_Remote = new IPEndPoint(IPAddress.Parse(textFriendsIp.Text), Convert.ToInt32(textFriendsPort.Text));
                sck.Connect(EP_Remote);

                //...Listening...
                byte[] buffer = new byte[1500];
                sck.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref EP_Local, new AsyncCallback(MessageCallBack), buffer);

                button1.Text = "Connected"; //start tuşu
                button1.Enabled = false; //bağlantı oluşturulduktan sonra başlat tuşuna basılamasın

                button2.Enabled = true;
                textMessage.Focus(); //mesaj yazmamızı istesin
            }

            catch(Exception exp)
            {
                MessageBox.Show(exp.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                //Textbox'ta yazılan mesajı bytelara dönüştürmeliyiz;
                System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
                byte[] msg = new byte[1500];
                msg = enc.GetBytes(textMessage.Text); //yazdığımız mesaj byte arraye dönüştürüldü

                sck.Send(msg); //mesajı clienta byte formunda gönder

                listBox1.Items.Add("You: " + textMessage.Text); //kendi mesajımızı listboxta görelim.
                textMessage.Clear();
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.ToString());
            }
        }
    }
}
